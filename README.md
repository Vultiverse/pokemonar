# Pokemon AR #

This Repository contains the Pokemon AR project I created! I hope you like it! Enjoy! Let me know in the comments on the video if you have additoinal questions not covered here.

### How To Use ###

1. Just download the repository to your computer and unzip it in the location of your choice. 
2. Make sure you have Unity installed
3. Go into PokemonAR/Pokemon AR/Assets/Scenes
4. Click on PokemonScene.unity

### Troubleshooting ###

### _Computer wont open the scene in Unity_ ###

1. Right click on PokemonScene.unity, click on Open with, then click Unity in the options below
2. Open Unity first, click on File, then Open Project, navigate to where you unzipped the project, and select Pokemon AR
3. If you have Unity Hub, repeat the same as step 2, when you click Open Project it will take you to Unity Hub. Click on Add, then follow the rest of step 2

### _I can't find the Bulbasaur asset_ ###

The bulbasaur asset is a paid asset. If you want to use it for your project you'll have to pay and download it from its creator. 

_The link is here: https://sketchfab.com/3d-models/bulbasaurs-nap-time-d1eac757cd234d85b8f010252f740417_