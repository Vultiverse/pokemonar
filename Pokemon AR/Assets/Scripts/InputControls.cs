// GENERATED AUTOMATICALLY FROM 'Assets/Scripts/InputControls.inputactions'

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Utilities;

public class @InputControls : IInputActionCollection, IDisposable
{
    public InputActionAsset asset { get; }
    public @InputControls()
    {
        asset = InputActionAsset.FromJson(@"{
    ""name"": ""InputControls"",
    ""maps"": [
        {
            ""name"": ""Rotator"",
            ""id"": ""44bac7cd-0e76-4eaf-b23c-62dbf1bf45e7"",
            ""actions"": [
                {
                    ""name"": ""Rotate Button"",
                    ""type"": ""Button"",
                    ""id"": ""1258d145-4295-468c-8bfb-593add3df8de"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                }
            ],
            ""bindings"": [
                {
                    ""name"": """",
                    ""id"": ""07ca934f-8374-41ce-8890-0b78e29e9b07"",
                    ""path"": ""<Touchscreen>/primaryTouch/tap"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Rotate Button"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                }
            ]
        }
    ],
    ""controlSchemes"": []
}");
        // Rotator
        m_Rotator = asset.FindActionMap("Rotator", throwIfNotFound: true);
        m_Rotator_RotateButton = m_Rotator.FindAction("Rotate Button", throwIfNotFound: true);
    }

    public void Dispose()
    {
        UnityEngine.Object.Destroy(asset);
    }

    public InputBinding? bindingMask
    {
        get => asset.bindingMask;
        set => asset.bindingMask = value;
    }

    public ReadOnlyArray<InputDevice>? devices
    {
        get => asset.devices;
        set => asset.devices = value;
    }

    public ReadOnlyArray<InputControlScheme> controlSchemes => asset.controlSchemes;

    public bool Contains(InputAction action)
    {
        return asset.Contains(action);
    }

    public IEnumerator<InputAction> GetEnumerator()
    {
        return asset.GetEnumerator();
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
        return GetEnumerator();
    }

    public void Enable()
    {
        asset.Enable();
    }

    public void Disable()
    {
        asset.Disable();
    }

    // Rotator
    private readonly InputActionMap m_Rotator;
    private IRotatorActions m_RotatorActionsCallbackInterface;
    private readonly InputAction m_Rotator_RotateButton;
    public struct RotatorActions
    {
        private @InputControls m_Wrapper;
        public RotatorActions(@InputControls wrapper) { m_Wrapper = wrapper; }
        public InputAction @RotateButton => m_Wrapper.m_Rotator_RotateButton;
        public InputActionMap Get() { return m_Wrapper.m_Rotator; }
        public void Enable() { Get().Enable(); }
        public void Disable() { Get().Disable(); }
        public bool enabled => Get().enabled;
        public static implicit operator InputActionMap(RotatorActions set) { return set.Get(); }
        public void SetCallbacks(IRotatorActions instance)
        {
            if (m_Wrapper.m_RotatorActionsCallbackInterface != null)
            {
                @RotateButton.started -= m_Wrapper.m_RotatorActionsCallbackInterface.OnRotateButton;
                @RotateButton.performed -= m_Wrapper.m_RotatorActionsCallbackInterface.OnRotateButton;
                @RotateButton.canceled -= m_Wrapper.m_RotatorActionsCallbackInterface.OnRotateButton;
            }
            m_Wrapper.m_RotatorActionsCallbackInterface = instance;
            if (instance != null)
            {
                @RotateButton.started += instance.OnRotateButton;
                @RotateButton.performed += instance.OnRotateButton;
                @RotateButton.canceled += instance.OnRotateButton;
            }
        }
    }
    public RotatorActions @Rotator => new RotatorActions(this);
    public interface IRotatorActions
    {
        void OnRotateButton(InputAction.CallbackContext context);
    }
}
