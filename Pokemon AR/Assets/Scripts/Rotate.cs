﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rotate : MonoBehaviour
{

    public float rotateSpeed; //Created a public variable to allow for the rotation speed to be adjusted 
    private InputControls inputActions; //Created a variable that will hold the input action asset "InputControls"
    private bool buttonPressed; //Creared a boolean variable that will hold true/false depending on whether the button has been pressed or not

    void Awake()
    {
        inputActions = new InputControls(); //Assigns the "InputControls" input action asset to the inputActions variable
        buttonPressed = false;
    }

    private void OnEnable()
    {
        inputActions.Enable(); //Enables the inputActionAsset (though it is enabled by default) when the function is called
    }

    private void OnDisable()
    {
        inputActions.Disable(); //Disables the inputActionAsset when the function is called
    }
 

    private void Update()
    {
        //charmanderRotate = new Vector3(0, 0, 0);
        
        if (inputActions.Rotator.RotateButton.triggered) //Checks whether the RotateButton action of the Rotator AssetMap within the "inputActions" inputActionAsset has been triggered
        {
            buttonPressed = !buttonPressed; //Sets the button value to true
        }

        if (buttonPressed) //Runs this function if the button has been pressed
        {
            gameObject.transform.Rotate(0, rotateSpeed * Time.deltaTime, 0); //Rotates the gameObject about the Y axis
        }

    }
}
